#!/bin/bash
#set -e

INSTALL='sudo xbps-install -y' # -n --dry-run
SYNC='sudo xbps-install -Sy'
UPDATE='sudo xbps-install -Su'

REPO_FOLDER='~/artix-sway'

KEY_BINDINGS='~/.config/sway/config.d/key-binding'

func_install() {
	if sudo xbps-query -o $1 &> /dev/null; then
		tput setaf 2
		echo $1 'is already Installed'
		tput sgr0
	else
		tput setaf 3
		echo '== Installing ==>' $1
		tput sgr0
		$INSTALL $1
	fi
}

SWAY_LIST=(
	sway
	swayidle
	swaylock
	Waybar
	slurp
	mako
	dmenu

	azote
	nwg-launchers
	wob

	kvantum
	qt5ct
	adwaita-qt

	polkit-gnome
	gnome-keyring
	gnome-themes-extra
	dconf-editor
	seahorse
	qt5-wayland
	flatpak

	zsh
	zsh-autosuggestions
	zsh-completions
	zsh-syntax-highlighting
	zsh-history-substring-search
	grml-zsh-config

	elogind
	dbus-elogind
	dbus-elogind-libs
	dbus-elogind-x11

	cronie
	bluez
	libvirt
	
	fakeroot
	make
	exfat-utils
	ntfs-3g
	xtools
	python3-i3ipc
	python-yaml

	connman-gtk
	pulseaudio
	pavucontrol
	pamixer
	blueman

	font-adobe-source-code-pro
	font-awesome5
	noto-fonts-ttf
	dejavu-fonts-ttf
)

tput setaf 6
echo '###############################################################################'
tput sgr0
tput setaf 4
echo '			Welcome to the Void SWAY Installer'
tput sgr0
tput setaf 6
echo '###############################################################################'
tput sgr0

func
PS3='Choose your Mirror: '
MIRROR=("USA: Chicago" "USA: Kansas City" "USA: New York" "EU: Finland")
select sel in "${MIRROR[@]}"; do
    case $sel in
    	"USA: Chicago")
            SET_MIRROR='https://mirrors.servercentral.com/voidlinux'
            echo ' Switching Mirror to' $SET_MIRROR
            break
            ;;
        "USA: Kansas City")
            SET_MIRROR='https://alpha.us.repo.voidlinux.org'
            echo ' Switching Mirror to' $SET_MIRROR
            break
            ;;
        "USA: New York")
            SET_MIRROR='https://mirror.clarkson.edu/voidlinux'
            echo ' Switching Mirror to' $SET_MIRROR
            break
            ;;
        "EU: Finland")
            SET_MIRROR='https://alpha.de.repo.voidlinux.org'
            echo ' Switching Mirror to' $SET_MIRROR
            break
            ;;
        *) echo ' Invalid option: '$REPLY ;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose your Repositories: '
REPO=("void-repo-nonfree" "void-repo-multilib" "void-repo-multilib-nonfree" "All" "Continue")
select sel in "${REPO[@]}"; do
    case $sel in
    	"void-repo-nonfree")
            	func_install void-repo-nonfree
            ;;
        "void-repo-multilib")
            	func_install void-repo-multilib
            ;;
        "void-repo-multilib-nonfree")
            	func_install void-repo-multilib-nonfree
            ;;
        "All")
            	repo_list=(void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree)
	                for name in "${repo_list[@]}" ; do
						func_install $name
					done
            	sudo mkdir -p /etc/xbps.d && 
            	sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/ &&
				sudo sed -i "s|https://alpha.de.repo.voidlinux.org|$SET_MIRROR|g" /etc/xbps.d/*-repository-*.conf &&
				$SYNC &&
	    	break
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    	sudo mkdir -p /etc/xbps.d &&
				sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/ &&
				sudo sed -i "s|https://alpha.de.repo.voidlinux.org|$SET_MIRROR|g" /etc/xbps.d/*-repository-*.conf &&
				$SYNC &&
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY ;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose your SWAY setup: '
DESKTOP=("SWAY" "SWAY with A minimal GNOME environment" "SWAY with GNOME desktop")
select sel in "${DESKTOP[@]}"; do
    case $sel in
        "SWAY")
    		sway_count=0
			for sway in "${SWAY_LIST[@]}" ; do
				sway_count=$[count+1]
				func_install $sway
			done
			break
            ;;
        "SWAY with A minimal GNOME environment")
			for sway in "${SWAY_LIST[@]}" ; do
				func_install $sway
			done
				func_install gnome-core
			break
            ;;
        "SWAY with GNOME desktop")
			for sway in "${SWAY_LIST[@]}" ; do
				func_install $sway
			done
				func_install gnome
    		break
            ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Copy "void-sway" files to $HOME DIRECTORY ?: ' 
CONFIG_FILES=("Config Files" "ZSH Files" "Wallpapers" "All" "Continue / DON'T COPY")
select sel in "${CONFIG_FILES[@]}"; do
    case $sel in
        "Config Files")
            cp -r $REPO_FOLDER/.config ~/ 2> /dev/null
			sed -i "s/dave/$USER/g" ~/.config/nwg-launchers/nwgbar/bar.json
			sed -i "s/dave/$USER/g" ~/.config/wofi/config
            ;;
        "ZSH Files")
            cp -r $REPO_FOLDER/.zprofile ~/ 2> /dev/null
            cp -r $REPO_FOLDER/.zshrc ~/ 2> /dev/null
            ;;
        "Wallpapers")
            cp -r $REPO_FOLDER/wallpapers ~/ 2> /dev/null
            ;;
        "All")
            cp -r $REPO_FOLDER/.config ~/ 2> /dev/null
            cp -r $REPO_FOLDER/.zprofile ~/ 2> /dev/null
            cp -r $REPO_FOLDER/.zshrc ~/ 2> /dev/null
            cp -r $REPO_FOLDER/wallpapers ~/ 2> /dev/null
			sed -i "s/dave/$USER/g" ~/.config/nwg-launchers/nwgbar/bar.json
			sed -i "s/dave/$USER/g" ~/.config/wofi/config
			break
            ;;
		"Continue / DON'T COPY")
		    echo ' Continuing install without files!!'
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose your Drivers: '
DRIVER=("Intel" "Nvidia" "Nvidia 300/400/500 Series" "Nvidia GeForce8/9 + 100/200/300 Series" "AMD / ATI" "Nouveau" "Continue with Install")
select sel in "${DRIVER[@]}"; do
    case $sel in
        "Intel")
			intel_list=(linux-firmware-intel intel-video-accel mesa-vulkan-intel vulkan-loader mesa-dri)
            for name in "${intel_list[@]}" ; do
				func_install $name
			done
			cat /sys/devices/cpu/caps/pmu_name
        	;;
        "Nvidia")
            func_install nvidia
            ;;
        "Nvidia 300/400/500 Series")
			func_install nvidia390
			;;
        "Nvidia GeForce8/9 + 100/200/300 Series")
			func_install nvidia340
			;;
		"AMD / ATI")
			amd_list=(mesa-dri vulkan-loader mesa-vulkan-radeon amdvlk)
            for name in "${amd_list[@]}" ; do
				func_install $name
			done
			;;
        "Nouveau")
			func_install nouveau
            ;;
		"Continue with Install")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose Terminals (Last one selected will be set in $KEY_BINDINGS as MOD+Return): '
TERMINAL=("Alacritty" "Gnome-terminal" "Continue")
select sel in "${TERMINAL[@]}"; do
    case $sel in
        "Alacritty")
            func_install alacritty
            ;;
        "Gnome-terminal")
            func_install gnome-terminal
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose File Managers (Last one selected will be set in $KEY_BINDINGS as MOD+Shift+Return): '
TERMINAL=("pcmanfm" "thunar" "nautilus" "Continue")
select sel in "${TERMINAL[@]}"; do
    case $sel in
        "pcmanfm")
            func_install pcmanfm
            ;;
        "thunar")
			thunar_list=(thunar thunar-archive-plugin thunar-media-tags-plugin thunar-volman)
	        for name in "${thunar_list[@]}" ; do
				func_install $name
			done
            ;;
		"nautilus")
			nautilus_list=(nautilus nautilus-sendto sushi)
	        for name in "${nautilus_list[@]}" ; do
				func_install $name
			done
            ;;
		"nemo")
			func_install nemo
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose App Launchers (key-bindings "wofi MOD+d" ULauncher "MOD+Shift+d"): '
LAUNCHER=("wofi" "ulauncher" "Continue")
select sel in "${LAUNCHER[@]}"; do
    case $sel in
        "wofi")
            func_install wofi
            ;;
        "ulauncher")
			func_install ulauncher
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose Web Browers (Last one selected will be set in $KEY_BINDINGS as MOD+F1): '
BROWSER=("firefox" "chromium" "Continue")
select sel in "${BROWSER[@]}"; do
    case $sel in
        "firefox")
            func_install firefox
            ;;
        "chromium")
			func_install chromium
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose Text Editors (Last one selected will be set in $KEY_BINDINGS as MOD+F3): '
EDITORS=("sublime-text3" "gedit" "mousepad" "vim" "Continue")
select sel in "${EDITORS[@]}"; do
    case $sel in
        "sublime-text3")
            func_install sublime-text3
            ;;
        "gedit")
			func_install gedit
            ;;
        "mousepad")
			func_install mousepad
            ;;
        "vim")
			func_install vim
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose Video Players (Last one selected will be set in $KEY_BINDINGS as MOD+F4): '
VIDEO=("Stremio" "Kodi" "Continue")
select sel in "${VIDEO[@]}"; do
    case $sel in
        "Stremio")
            func_install stremio-shell
            ;;
        "Kodi")
			func_install kodi
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose VM Software (Last one selected will be set in $KEY_BINDINGS as MOD+F5): '
VM=("qemu" "virt-manager" "Continue")
select sel in "${VM[@]}"; do
    case $sel in
        "qemu")
            func_install stremio-shell
            ;;
        "virt-manager")
			func_install kodi
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Choose Torrent Software (Last one selected will be set in $KEY_BINDINGS as MOD+F6): '
TORRENT=("Transmission" "Continue")
select sel in "${TORRENT[@]}"; do
    case $sel in
        "Transmission")
            func_install transmission-gtk
            ;;
		"Continue")
		    echo ' Continuing Installation '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

tput setaf 2
echo '#### Enabling services ####'
tput sgr0

	#sudo rm /var/service/lightdm

	sudo ln -s /etc/sv/dbus /var/service/
	sudo ln -s /etc/sv/elogind /var/service/
	sudo ln -s /etc/sv/connmand /var/service/
	sudo ln -s /etc/sv/virtlockd /var/service/
	sudo ln -s /etc/sv/virtlogd /var/service/
	sudo ln -s /etc/sv/libvirtd /var/service/
	#sudo ln -s /etc/sv/wpa_supplicant /var/service/

	sudo rm /var/service/dhcpcd
	sudo rm /var/service/acpid

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Do you want to Change your Shell to ZSH ?: '
SHELL=("ZSH for $USER" "ZSH for root" "ZSH for Both" "Continue with Bash")
select sel in "${SHELL[@]}"; do
    case $sel in
        "ZSH for $USER")
			chsh -s /bin/zsh
            break
            ;;
        "ZSH for root")
            sudo chsh -s /bin/zsh
            break
            ;;
        "ZSH for Both")
			chsh -s /bin/zsh
            sudo chsh -s /bin/zsh
            break
            ;;
		"Continue with Bash")
		    echo ' Keeping Bash Shell '
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY;;
    esac
done

echo ''
tput setaf 6
echo '###############################################################################'
tput sgr0
echo ''

PS3='Do you want to enable Autologin on TTY1 ?: '
LOGIN=("Name only Autologin" "Full Autologin" "Continue without Autologin")
select sel in "${LOGIN[@]}"; do
    case $sel in
        "Name only Autologin")
            sudo sed -i "s/--noclear/--skip-login --login-options $USER --noclear/g" /etc/sv/agetty-tty1/conf
            echo ' Install done Reboot your system '
            break
            ;;
        "Full Autologin")
            sudo sed -i "s/-a $USER --noclear/g" /etc/sv/agetty-tty1/conf
            echo ' Install done Reboot your system '
            break
            ;;
		"Continue without Autologin")
			tput setaf 8;
		    echo ' Install done Reboot your system '
		    echo;tput sgr0
		    break
		    ;;
        *) echo ' Invalid option: '$REPLY
			;;
    esac
done